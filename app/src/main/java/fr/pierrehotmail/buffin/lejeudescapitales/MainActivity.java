package fr.pierrehotmail.buffin.lejeudescapitales;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int score=0;
    String resp;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        NewQuestion();

    }

    public void NewQuestion(){
        TextView monTexte = (TextView) findViewById(R.id.txquestion);
        Button btnRespA = (Button)  findViewById(R.id.btnRespA);
        Button btnRespB = (Button)  findViewById(R.id.btnRespB);
        Button btnRespC = (Button)  findViewById(R.id.btnRespC);
        Button btnRespD = (Button)  findViewById(R.id.btnRespD);
        TextView txscore = (TextView)  findViewById(R.id.txscore);


        Question question = new Question();

        monTexte.setText("Quelle est la capitale " + question.quest);
        btnRespA.setText(question.caseA);
        btnRespB.setText(question.caseB);
        btnRespC.setText(question.caseC);
        btnRespD.setText(question.caseD);
        resp = question.resp;
        txscore.setText(""+score);

        btnRespA.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
             if(resp== "A"){
                 score = score +1;
             }
                NewQuestion();
            }

        });
        btnRespB.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(resp== "B"){
                    score = score +1;
                }
                NewQuestion();
            }
        });
        btnRespC.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(resp== "C"){
                    score = score +1;
                }
                NewQuestion();
            }
        });
        btnRespD.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(resp== "D"){
                    score = score +1;
                }

                NewQuestion();

            }
        });
    }

}
