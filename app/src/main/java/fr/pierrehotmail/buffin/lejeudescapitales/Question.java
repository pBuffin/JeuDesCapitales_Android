package fr.pierrehotmail.buffin.lejeudescapitales;

/**
 * Created by Admin on 23/08/2017.
 */

public class Question {

    private String[][] question = {
            { " de l'Albanie?", "Manama", "Tirana", "Vienne", "Minsk", "B"},
            { " de l'Algérie?", "Zagreb", "Moroni", "Alger", "Séoul", "C"},
            { " de l'Allemagne?", "Berlin","Chisinau","Islamabad","Honiara","A"},
            {" de l'Angola?","Koweït","Kingston","Palikir","Luanda","D"},
            {" de l'Arabie saoudite?","Asuncion","Riyad","Monteveo","Paramaribo","B"},
            {" de l'Argentine?","Buenos Aires","Moscou","Doha","Tachkent","A"},
            {" de l'Australie?","Katmandou","Lilongwe","Canberra","Monrovia","C"},
            {" de l'Autriche?","Port-Louis","Bucarest","Prague","Vienne","D"},
            {" de l'Azerbaïdjan?","Paramaribo","Bakou","Bratislava","Kigali","B"},
            {" du Bangladesh?","Dacca","Wellington","Tachkent","Achgabat","A"},
            {" de la Belgique?","Castries","Apia","Honiara","Bruxelles","D"},
            {" de la Biélorussie?","Prague","Nuku alofa","Minsk","Harare","C"},
            {" de la Birmanie?","Naypyaw","Santiago","Copenhague","San José","A"},
            {" de la Bolivie?","Kigali","Londres","Varsovie","La Paz","D"},
            {" de la Bosnie-Herzégovine?","Tallinn","Sarajevo","Asmara","Budapest","B"},
            {" du Botswana?","Rome","Zagreb","Gaborone","Moroni","C"},
            {" du Brésil?","Helsinki","Bichkek","Mexico","Brasilia","D"},
            {" de la Bulgarie?","Sofia","Niamey","Kiev","Vilnius","A"},
            {" du Burkina Faso?","Asmara","Ouagadougou","Yamoussoukro","Lilongwe","B"},
            {" du Cambodge?","Skopje","Vaduz","Islamabad","Phnom Penh","D"},
            {" du Cameroun?","Kigali","Niamey","Yaoundé","Achgabat","C"},
            {" du Canada?","Ottawa","Brgetown","Nassau","Sucre","A"},
            {" de la République centrafricaine?","Banjul","Bangui","Monrovia","Bissau","B"},
            {" du Chili?","Managua","Tegucigalpa","Roseau","Santiago","D"},
            {" de la Chine?","Pékin","Séoul","Tokyo","New Delhi","A"},
            {" de Chypre?","Sofia","Nicosie","Tallinn","Bruxelles","B"},
            {" de la Colombie?","Santiago","San José","Nicosie","Bogota","D"},
            {" de la Corée du Nord?","Séoul","Pékin","Pyongyang","Banjul","C"},
            {" Corée du Sud?","Beyrouth","Pékin","Pyongyang","Séoul","D"},
            {" de la Côte d Ivoire?","Yamoussoukro","Malabo","Bamako","Kigali","A"},
            {" de la Croatie?","Tirana","Zagreb","Tallinn","Pristina","B"},
            {" de Cuba?","Vilnius","Nassau","San José","La Havane","D"},
            {" du Danemark?","Oslo","Stockholm","Copenhague","Bucarest","C"},
            {" de l'Égypte?","Le Caire","Maputo","Kinshasa","São Tomé","A"},
            {" des Émirats arabes unis?","Abuja","Abou Dabi","Niamey","Kampala","B"},
            {" de l'Équateur?","Tripoli","Monteveo","Caracas","Quito","D"},
            {" de l'Espagne?","Nicosie","Caracas","Madr","Quito","C"},
            {" de l'Estonie?","Vilnius","Varsovie","Bucarest","Tallinn","D"},
            {" des États-Unis?","Washington D.C.","Saint-Georges","Bruxelles","Pyongyang","A"},
            {" de l'Éthiopie?","Windhoek","Addis-Abeba","Kigali","Dakar","B"},
            {" de la Finlande?","Helsinki","Copenhague","Dublin","Oslo","A"},
            {" de la France?","Moscou","Amsterdam","La Valette","Paris","D"},
            {" du Gabon?","Banjul","Niamey","Libreville","Kinshasa","C"},
            {" du Ghana?","Conakry","Accra","Thimbu","Tbilissi","B"},
            {" de Haïti?","Podgorica","Melekeok","Dili","Port-au-Prince","D"},
            {" du Honduras?","Tegucigalpa","Lima","Asunción","San José","A"},
            {" de la Hongrie?","Canberra","Vilnius","Budapest","Pristina","B"},
            {" l Inde?","Jakarta","Manama","New Delhi","Amman","C"},
            {" de l'Irak?","Astana","Téhéran","Banjul","Bagdad","D"},
            {" de l'Iran?","Téhéran","Mascate","Doha","Bagdad","A"},
            {" de l'Irlande?","Brgetown","Dublin","Ottawa","Reykjavik","B"},
            {" de l'Italie?","Paris","Athènes","Rome","Dublin","C"},
            {" de la Jamaïque?","Amman","Nairobi","Vientiane","Kingston","D"},
            {" de la Lettonie?","Skopje","Riga","Malé","Monrovia","B"},
            {" du Liban?","Beyrouth","Vaduz","Tallinn","Chisinau","A"},
            {" de la Libye?","Antananarivo","Varsovie","La Valette","Tripoli","D"},
            {" du Mali?","Katmandou","Dodoma","Bamako","Beyrouth","C"},
            {" du Maroc?","Rabat","Alger","Tunis","Dakar","A"},
            {" de Malte?","Dublin","La Valette","Conakry","Amman","B"},
            {" du Mexique?","Tegucigalpa","San José","Banjul","Mexico","D"},
            {" de la Mongolie?","Banjul","Lima","Tbilissi","Oulan-Bator","D"},
            {" du Népal","Katmandou","Windhoek","Kigali","Apia","A"},
            {" du Nicaragua","Chisinau","Nouakchott","Managua","Palikir","C"},
            {" du Niger","Niamey","Abuja","Khartoum","Doha","B"},
            {" de la Norvège","Victoria","Stockholm","Copenhague","Oslo","D"},
            {" de la Nouvelle-Zélande","Wellington","Canberra","Freetown","Victoria","A"},
            {" du Paraguay","Lima","Monteveo","Asuncion","Tegucigalpa","C"},
            {" des Pays-Bas","Apia","Amsterdam","Bucarest","Londres","B"},
            {" du Pérou","Monteveo","Asunción","Managua","Lima","D"},
            {" de la Pologne","Reykjaík","Skopje","Varsovie","Pristina","C"},
            {" du Portugal","Lisbonne","Madr","Dublin","Bucarest","A"}
    };
    int randomNum = 1 + (int)(Math.random() * 71);
    String quest;
    String caseA;
    String caseB;
    String caseC;
    String caseD;
    String resp;

    public Question(){

        quest = question[randomNum][0];
        caseA = question[randomNum][1];
        caseB = question[randomNum][2];
        caseC = question[randomNum][3];
        caseD = question[randomNum][4];
        resp = question[randomNum][5];
    }

}
